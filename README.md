# README #

These slides explain how to set up GUI support for git in Ubuntu using RabbitVCS. It covers:

1. Installing Git and telling Git who you are.
2. Adding Git support to Ubuntu's file manager using RabbitVCS.
3. Setting Up ssh in Bitbucket to work with the local system.

The .fodp file is a slide show presentation that can be opened using LibreOffice.

The .pdf file has the exact same slides converted to pdf format.
You can [view the pdf file](https://bitbucket.org/dmackerman/git_ubuntu_instructions/raw/master/git_ubuntu.pdf?at=master&fileviewer=file-view-default) without cloning the repository.